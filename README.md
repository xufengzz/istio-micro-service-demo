什么是云原生？ 
云原生是基于分布部署和统一运管的分布式云 以容器、微服务、DevOps等技术为基础建立的一套云技术产品体系。
这里以K8s作为注册中心。K8s service作为服务间负载均衡器。网关使用k8s中的ingress(也可以用别的)，所以原则上实现云原生。只需要spring boot + rpc框架即可。rpc直接使用服务名访问。
# 基于Spring Boot+Istio的Service Mesh微服务架构示例代码
```
修改父pom里面的阿里云仓库信息为你的
使用idea 右边的maven打包构建
指定某个项目 如micro-api  micro-order micro-pay运行 
mvn clean install package  -P prod  dockerfile:build dockerfile:push 生成三个镜像进行到阿里云 
```
使用doc目录的K8s部署文件 部署到k8s中。暴露网关地址即可。即可实现服务间负载均衡。多副本。灰度发布。 

模拟App客户端服务调用的服务架构，其调用链路如下：

           micro-api(面向外部客户端的Api服务)
                  |
                  | http协议
                  |
             micro-order(内部订单服务)
                  |
                  | Grpc协议
                  |
              mciro-pay(内部支付服务)
              
如上所示链路，具体说明如下：

1)、为了完整演示在Service Mesh架构下的微服务研发过程，这里我们定义三个微服务，其中micro-api服务是面向外部客户端的接入Api服务提供Http协议访问；

2）、而micro-api与micro-order则基于微服务的注册发现机制进行内部微服务调用访问，采用Http协议；

3）、micro-order与micro-pay之间也基于微服务注册发现机制进行内部微服务调用访问，为了演示多种场景，这里两个微服务的调用采用GRpc协议;  

最简单实现云原生的案例
kubesphere 自制应用
![img.png](doc/1.png)


//部署了两个pay服务
![img.png](doc/4.png)
//两个容器日志都有记录 说明istio负载均衡没问题
![img.png](doc/3.png)

金丝雀部署
![img.png](doc/2.png)
更详细的文章说明链接：
https://mp.weixin.qq.com/s/L1LoiI9NZqwZWsuCzEJN1A    

https://mp.weixin.qq.com/s/o2SrM7yrK9Kja2B40U63Ug


来自这个开源仓库
https://github.com/manongwudi/istio-micro-service-demo
