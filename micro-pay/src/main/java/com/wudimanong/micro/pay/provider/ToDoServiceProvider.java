package com.wudimanong.micro.pay.provider;

import com.wudimanong.micro.pay.proto.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author xufeng
 * @description: TODO
 * @date 2022-04-13 11:31
 */
@Slf4j
@Component
public class ToDoServiceProvider extends ToDoServiceGrpc.ToDoServiceImplBase {
    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void create(CreateRequest request, StreamObserver<CreateResponse> responseObserver) {
        //逻辑处理(简单模拟打印日志)
        log.info("处理gRPC支付处理请求,{}", request.toString());
        //构建返回对象(构建处理状态)
        CreateResponse response = CreateResponse.newBuilder().setApi("2").build();
        //设置数据响应
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void read(ReadRequest request, StreamObserver<ReadResponse> responseObserver) {
        super.read(request, responseObserver);
    }

    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void update(UpdateRequest request, StreamObserver<UpdateResponse> responseObserver) {
        super.update(request, responseObserver);
    }

    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void delete(DeleteRequest request, StreamObserver<DeleteResponse> responseObserver) {
        super.delete(request, responseObserver);
    }

    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void readAll(ReadAllRequest request, StreamObserver<ReadAllResponse> responseObserver) {
        super.readAll(request, responseObserver);
    }
}
